;;; org-brew.el --- org-mode extensions for brewing ber
;;
;; Copyright (C) 2009 Erik Hetzner
;;
;; Author: Erik Hetzner <ehetzner@gmail.com>
;;
;; This file is NOT part of GNU Emacs.
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;; calc functions for brewing

(require 'org-cook)
(require 'brew-calc)

(defun org-brew-get-aaus ()
  (interactive)
  (let* ((item (org-cook-parse-ingredient-at-point))
         (amount (car item))
         (ingredient (car (cdr item)))
         (amount-in-oz (math-remove-units
                        (math-convert-units
                         (math-read-expr amount)
                         (math-read-expr "1 oz")))))
    (save-excursion
      (org-link-search ingredient)
      (let* ((aa (math-read-number
                 (cdr (assoc "AA" (org-entry-properties)))))
             (aau (math-mul amount-in-oz aa)))
        (message "AAUs: %s" (math-format-number aau))))))

(defun org-brew-convert-ingredient (food-ingr)
  (let* ((item (cdr (assq :item food-ingr)))
         (amount-calc (cdr (assq :amount-calc food-ingr)))
         (unit (math-units-in-expr-p amount-calc t)))
    (cond ((or (eq (car unit) 'lb)
               (eq (car unit) 'kg))
           ;; grain
           (append food-ingr
                   '((:brew-ingr-type . :grain))))
          ((or (eq (car unit) 'oz)
               (eq (car unit) 'g))
           ;; hop
           (let* ((time-str
                   (or (org-entry-get (point) "boil_time")
                       (and (string-match "\\([0-9]+\\) \\(m\\.\\|min\\)" item)
                            (match-string 1 item))))
                  (time (if time-str (string-to-int time-str)))
                  (alpha-str
                   (or (org-entry-get (point) "alpha")
                       (and (string-match "\\([0-9\\.]+\\)%" item)
                            (match-string 1 item))))
                  (alpha (if alpha-str (string-to-number alpha-str))))
             (append food-ingr
                     `((:brew-ingr-type . :hop)
                       ,@(if time
                             (list (cons :boil-time time)))
                       ,@(if alpha
                             (list (cons :alpha alpha))))))))))
  
(defun org-brew-parse-ingredient-at-point ()
  (org-brew-convert-ingredient (org-cook-parse-ingredient-at-point)))

(defun org-brew-get-ingredient-list ()
  (let ((food-ingr-list (org-cook-get-recipe-ingredient-list)))
    (mapcar (lambda (ingr)
              (org-brew-convert-ingredient ingr))
            food-ingr-list)))

;(defun org-brew-calc-recipe-ibu ()
;  (interactive)
;  (let ((ingr-list (org-brew-get-ingredient-list))
;        (ibu 0))
;    (mapc (lambda (ingr)
;            (if (eq :hop (cdr (assq :brew-ingr-type ingr)))
;                (let ((time (
;                (setq ibu (+ ibu
;                             (calcFunc-brewTinsethIBU 

(defun org-brew-visit-ingredient (ingr)
  (let ((ingr-name (replace-regexp-in-string
                    "\\(([^)]+)\\|[^A-Za-z() ]\\|AA\\)" "" (cdr (assq :item ingr)))))
    (message "%s" ingr-name)
    (org-open-file "/home/egh/w/org-brew/beersmith-hops.org" t nil
                   (format "*%s" ingr-name))))

(defun org-brew-visit-ingredient-at-point ()
  (interactive)
  (org-brew-visit-ingredient
   (org-brew-parse-ingredient-at-point)))

