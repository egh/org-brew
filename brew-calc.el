;;; brew-calc.el --- calc functions for brewing ber
;;
;; Copyright (C) 2009 Erik Hetzner
;;
;; Author: Erik Hetzner <ehetzner@gmail.com>
;;
;; This file is NOT part of GNU Emacs.
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;; calc functions for brewing

(provide 'brew-calc)
(require 'calc)
(require 'org-cook)

(org-cook-alist-append 'math-additional-units
                       '((degPlato nil "Degrees Plato")))

(setq math-units-table nil)

(defmath brewNorm (v)
  "Get the norm of a value with units."
  (math-simplify (math-remove-units v)))

(defmath brew2F (temp)
  "Ensure that a temperature is in degF."
  (let ((old-units (math-units-in-expr-p temp t)))
    (math-simplify
     (cond ((not old-units)
            (error "Not a temperature."))
           ((eq (car old-units) 'degF)
            temp)
           ((eq (car old-units) 'degC)
            (math-convert-temperature temp '(var degC var-degC) '(var degF var-degF)))
           ((eq (car old-units) 'K)
            (math-convert-temperature temp '(var K var-K) '(var degF var-degF)))))))

(defmath brew2C (temp)
  "Ensure that a temperature is in degC."
  (let ((old-units (math-units-in-expr-p temp t)))
    (math-simplify
     (cond ((not old-units)
            (error "Not a temperature."))
           ((eq (car old-units) 'degC)
            temp)
           ((eq (car old-units) 'degF)
            (math-convert-temperature temp '(var degF var-degF) '(var degC var-degC)))
           ((eq (car old-units) 'K)
            (math-convert-temperature temp '(var K var-K) '(var degC var-degC)))))))

(defmath brew2K (temp)
  "Ensure that a temperature is in degK."
   (let ((old-units (math-units-in-expr-p temp t)))
     (math-simplify
      (cond ((not old-units)
             (error "Not a temperature."))
            ((eq (car old-units) 'degC)
             (math-convert-temperature temp '(var degC var-degC) '(var K var-K)))
            ((eq (car old-units) 'K)
             temp)
            ((eq (car old-units) 'degF)
             (math-convert-temperature temp '(var degF var-degF) '(var K var-K)))))))

(defmath brew2MG_L (expr)
  "Ensure that a mass / vol is in mg / L."
  (math-convert-units 
   expr
   '(/ (var mg var-m)
       (var L var-L))))

(defmath brew2min (expr)
  "Ensure that a time is in minutes."
  (math-convert-units expr '(var min var-min)))

;; From Wikipedia, Plato_scale
(defmath brewSG2P (sg)
  "Convert from Specific gravity to Plato, correcting SG vector
if necessary."
  (let ((sg1 (brewSGCorr sg)))
    (* (+ :"-616.868"
          (+ (* :"1111.14" sg1)
             (+ (* :"-630.272"
                   (^ sg1 2))
                (+ (* :"135.997"
                      (^ sg1 3))))))
       '(var degPlato var-degPlato))))
 
;; From Wikipedia, Plato_scale
(defmath brewP2SG (p)
  "Convert Plato (degPlato) to specific gravity (unitless)."
  (let ((pn (brewNorm p)))
    (math-simplify (/ (- 668
                         (nroot (- (^ 668 2)
                                   (* 820 
                                      (+ 463 pn)))
                                2))
                      410))))

(defmath brew2P (n)
  "Ensure that a value is in degrees Plato (degPlato)."
  (if (eq (car (math-units-in-expr-p n t)) 'degPlato)
      n
    (brewSG2P n)))

(defmath brew2SG (n)
  "Ensure that a value is in corrected, unitless, specific
gravity points."
  (if (eq (car (math-units-in-expr-p n t)) 'degPlato)
      (brewP2SG n)
    (brewSGCorr n)))

;;(defmath brewFpn (x)
;;  (let ((Pn (brewNorm (brew2P x))))
;;    (+ :"0.48394"
;;       (+ (* :"0.0024688" Pn)
;;          (* :"0.00001561" (^ Pn 2))))))

;;(defmath brewAW (P n)
;;  (let ((P1 (brewNorm (brew2P P)))
;;        (n1 (brewNorm (brew2P n))))
;;    (* (brewFpn P1)
;;       (- P1 n1))))

(defmath brewAW2AV (aw ae)
  "Convert Alcohol by weight to alcohol by volume."
  (* aw (/ (brew2SG ae) :"0.79661")))

(defmath brewAV (oe ae)
  "Calculate alc by vol. given original extract and apparent
final extract."
  (brewAW2AV
   (brewAWFix oe ae) ae))

;; From George Fix, http://hbd.org/hbd/archive/880.html#880-9, due to
;; Balling.
(defmath brewRE (oe ae)
  "Calculate real extraction, given original extract and apparent
final extract."
  (let ((oe1 (brewNorm (brew2P oe)))
        (ae1 (brewNorm (brew2P ae))))
    (math-simplify
     (* (+ (* :"0.1808" oe1)
           (* :"0.8192" ae1))
        '(var degPlato var-degPlato)))))

(defmath brewAWFix (oe ae)
  "Calculate alc. by weight, given original extract and apparent
final extract."
  (let* ((oe1 (brewNorm (brew2P oe)))
         (ae1 (brewNorm (brew2P ae)))
         (re (brewNorm (brewRE oe ae))))
    (/ (- oe1 re)
       (- :"2.0665"
          (* :"0.010665" oe1)))))

(defmath brewCalories (oe ae)
  "Calculate cal/L, given original extract and apparent final
extract."
  (let* ((fg (brew2SG ae))
         (a (brewAWFix oe ae))
         (re (brewNorm (brewRE oe ae))))
    (* (* (* (+ (* :"6.9" a)
                (* :"4.0" (- re :".1")))
             fg)
          10)
       (/ '(var Kcal var-Kcal)
          '(var L var-L)))))

(defmath brewRA (oe ae)
  "Calculate approximation to real attenuation."
  (let ((re (brewNorm (brewRE oe ae)))
        (oe1 (brewNorm (brew2P oe))))
    (- 1 (/ re oe1))))

(defmath brewAA (oe ae)
  "Calculate approximate attenuation."
  (let ((ae1 (brewNorm (brew2P ae)))
        (oe1 (brewNorm (brew2P oe))))
    (- 1 (/ ae1 oe1))))


;; Per http://www.csgnetwork.com/h2odenscalc.html and
;; http://www.earthwardconsulting.com/density.xls
(defmath brewRhoCalc (temp)
  "Calculate factor by which water at a given temp differs from
water at ref. temp (~4 degC)"
  (let ((tempn (brewNorm (brew2C temp))))
    (math-simplify
     (- 1 (* (/ (+ tempn :"288.9414") 
                (* :"508929.2"
                   (+ tempn :"68.12963")))
             (^ (- tempn :"3.9863") 2))))))

;; Follows from above, assuming that beer has the same relationship to
;; temperature as water does.
(defmath brewSGCorr (sg)
  "Ensures that specific gravity is in a corrected, unitless
number, rather than a vector containing measured gravity, temp of
sampled liquid, and calibration temp, e.g. [1.010, 74 degF, 20
degC]"
  (if (not (vectorp sg))
      sg
    (let ((ir (nth 1 sg))
          (it (nth 2 sg))
          (ct (nth 3 sg)))
      (/ (* (brewRhoCalc ct) ir)
         (brewRhoCalc it)))))

(defmath brewTinsethBigness (ex)
  "Calculate the Tinseth 'bigness' AA utilization factor."
  (let ((sg (brew2SG ex)))
    (* :"1.65"
       (^ :"0.000125"
          (- sg 1)))))

(defmath brewTinsethBoilTime (time)
  "Calculate the Tinseth 'boil time' AA utilization factor."
  (let ((min (brewNorm (brew2min time))))
    (/ (- 1 (^ e (* min :"-0.04")))
       :"4.15")))

(defmath brewTinsethAAUtil (ex m)
  "Calculate the Tinseth AA utilization factor."
  (* (brewTinsethBigness ex)
     (brewTinsethBoilTime m)))

(defmath brewTinsethIBU (time hop-mass aa boil-size ex)
  "Calculate the IBUs given a time, mass of hops, aa of hops,
size of boil, and extract (SG or Plato)."
  (* (brewTinsethAAUtil ex time)
     (brew2MG_L
      (/ (* aa hop-mass) boil-size))))

;; See http://en.wikipedia.org/wiki/Henry%27s_Law. For CO_2.
(defmath henryConst (temp1)
  (let ((temp_st (* 298 '(var degK var-degK)))
        (temp (brew2K temp1))
        (k_h '(/ (* (float 294 -1) (* (var L var-L) (var atm var-atm))) 
                 (var mol var-mol))))
    (math-simplify (* k_h (^ e (* -2400
                                  (- (/ 1 (math-remove-units temp))
                                     (/ 1 (math-remove-units temp_st)))))))))

(defmath brewCarb (temp)
  "Calculate equilibrium carbonation at 1 atm."
  (brew2Volumes
   (let ((k_h (henryConst temp))
         (p (* 1 '(var atm var-atm))))
     (/ p k_h))))

(defmath brew2Volumes (expr)
  "Converts a gas in mol / L to volumes."
  (math-simplify-units
   (let ((V0 (math-convert-units '(var V0 var-V0)
                                 '(/ (var L var-L)
                                     (var mol var-mol)))))
     (* V0 expr))))

(defmath brew2molPerL (expr)
  (math-simplify-units
   (let ((V0 (math-convert-units '(var V0 var-V0)
                                 '(/ (var L var-L)
                                     (var mol var-mol)))))
     (/ expr V0))))
  
(defmath brewCO2gPerL (expr)
  "Calculates g/L of CO2, given mol/L or volumes."
  (let ((mm '(/ (* (float 4401 -2)
                   (var g var-g))
                (var mol var-mol)))
        (molPerL (if (math-units-in-expr-p expr t)
                     expr
                   (brew2molPerL expr))))
    (math-simplify-units (* molPerL mm))))

;; From http://oz.craftbrewer.org/Library/Methods/BulkPriming/TechnicalGuide.shtml
(defmath brewGramsGlucoseNeeded (expr)
  "Calculate the grams of glucose needed per g CO_2 desired."
  (* expr :"2.16"))

(defmath brewPrimingCalc (final-level1 temp volume)
  "Given a final CO_2 level (in volumes or g/L), a temperature,
and a volume of beer, return grams of glucose necessary for
priming."
  (let ((final-level (if (math-units-in-expr-p final-level1 t)
                         final-level1
                       (brewCO2gPerL final-level1)))
        (current-level (brewCO2gPerL (brewCarb temp))))
    (math-simplify-units
     (math-simplify
      (brewGramsGlucoseNeeded (* (- final-level current-level)
                                 volume))))))

(defvar brew-calc-malt-default-specific-heat
  (math-read-number "0.40")
  "The default value for the specific heat of malt; can vary from
0.38 to 0.42 depending on moisture.")

(defvar brew-calc-room-temp
  '(* 74 (var degF var-degF)))
  

(defmath brewCalcStrike (temp water-vol malt)
  (brewCalcStrikeFull temp water-vol malt brew-calc-malt-default-specific-heat brew-calc-room-temp))

(defmath brewCalcStrikeFull (temp1 water-vol malt malt-sh malt-temp1)
  (let* ((temp (brew2K temp1))
         (water (* water-vol (/ '(var kg var-kg) '(var L var-L))))
         (water-sh 1)
         (malt-temp (brew2K malt-temp1)))
    (brew2F (math-simplify-units
             (/ (- (* temp (+ (* water water-sh) 
                              (* malt malt-sh)))
                   (* malt-temp (* malt malt-sh)))
                (* water-sh water))))))
